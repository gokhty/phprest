select * from boleta;
select * from producto_has_boleta;
select * from producto;
drop trigger reduceStock;
delimiter |
create trigger reduceStock
after
insert on producto_has_boleta for each row
begin
declare getCantPro int;
declare getCodPro int;
declare getStockPro int;
declare getCodBol int;
set getCodBol = (select idBoleta from boleta  order by idBoleta desc limit 1);
set getCodPro = (select idProducto from producto_has_boleta  order by idBoleta desc limit 1);
set getCantPro = (select cantidad from producto_has_boleta where idProducto = getCodPro order by idBoleta desc limit 1);
set getStockPro = (select stock from producto where idProducto = getCodPro);
if(getCantPro <= getStockPro)then
update producto set stock = stock - getCantPro where idProducto = getCodPro;
else
delete from producto_has_boleta where idBoleta = getCodBol;
delete from boleta where idBoleta = getCodBol;
end if;
end
| 

insert into boleta (fecha,idCliente,idUsuario) values 
(curdate(), 1, 1);

insert into producto_has_boleta values
(5, 1, 2.5, 3000),
(2, 5, 1, 9);


call actualizaProducto(1);














delimiter |
create trigger reduceStock
after
insert on boleta for each row
begin
declare getCodBol int;
set getCodBol = (select idBoleta from boleta  order by idBoleta desc limit 1);
call actualizaProducto(getCodBol);
end
| 








delimiter |
 create procedure actualizaProducto(p_codBol int)
 begin
 DECLARE done BOOLEAN DEFAULT FALSE;
DECLARE a int;
DECLARE b int;
DECLARE c int;
declare d int;
 DECLARE c1 CURSOR FOR
       SELECT idBoleta, idProducto, cantidad
         FROM producto_has_boleta
        WHERE idBoleta = p_codBol;
        
        
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = TRUE;
     open c1;
	  	c1_loop: LOOP
			fetch c1 into a,b,c;
         IF done THEN LEAVE c1_loop; END IF; 
   set d = (select stock from producto where idProducto = b);
   if(c <= d)then
   update producto set stock = stock - c where idProducto = b;
   else
   delete from producto_has_boleta where idBoleta = a;
delete from boleta where idBoleta = a;
   end if;
		END LOOP c1_loop;
	CLOSE c1;
 
 end
 |