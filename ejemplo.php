<?php

//ejemplo get
// Crear un nuevo recurso cURL
$ch = curl_init();

// Configurar URL y otras opciones apropiadas
curl_setopt($ch, CURLOPT_URL, "http://localhost/phprest/productos.php");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);

// Capturar la URL y pasarla al navegador
curl_exec($ch);

// Cerrar el recurso cURL y liberar recursos del sistema
curl_close($ch);

// ----------------------------------------------------------------------------------------------
/*
//ejemplo post con parametros
//obtener valores de angularjs
$pd = file_get_contents("php://input");
$rq = json_decode($pd);
$des = $rq->aaa;
$des1 = $rq->abc;
$ch = curl_init('http://localhost/phprest/loginn.php');
curl_setopt ($ch, CURLOPT_POST, 1);
curl_setopt ($ch, CURLOPT_POSTFIELDS, "usu=$des&pass=$des1");
curl_setopt($ch,CURLOPT_RETURNTRANSFER,false);
curl_exec ($ch);
curl_close ($ch);
*/
// ----------------------------------------------------------------------------------------------
/*
//ejemplo get con parametros
$ch = curl_init();
$edad = $_GET["edad"];
$genero = $_GET["genero"];
// Configurar URL y otras opciones apropiadas
curl_setopt($ch, CURLOPT_URL, "http://localhost/phprest/ofertadia.php?edad=$edad&genero=$genero");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);

// Capturar la URL y pasarla al navegador
curl_exec($ch);

// Cerrar el recurso cURL y liberar recursos del sistema
curl_close($ch);
*/
?>