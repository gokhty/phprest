drop database caja;
create database caja;
use caja;
create table usuarios(
id int primary key auto_increment,
nombre varchar(45),
correo varchar(45),
dni char(8),
usu varchar(45),
pass varchar(45)
);
create table cajas(
id_caja int primary key auto_increment
);
create table reservaCajas(
fkusu int,
tiempo int, -- tiempo en el que el cliente va ha terminar de comprar
horaAtencion time -- suma de tiempo + hora actual
);
/*
-- obtener una caja según el tiempo que establece el usuario
-- el último atendido - el siguiente después de este: si el resultado es menor a tiempo del siguiente+1 antentido que se antienda antes de este, sino que se antienda despúes
*/
/*
el usuario al escoger prorroga le dan una caja con tiempo mayor a la ultima reservada(ver todas las cajas reservadas)
*/


-- ------------------------------------------------------------

delimiter |
create procedure sp_regusu(pnom varchar(45), pcor varchar(45), pdni char(8), pusu varchar(45), ppass varchar(45))
begin
insert into usuarios(nombre,correo,dni,usu,pass)values
(pnom,pcor,pdni,pusu,ppass);
end
|

-- ------------------------------------------------------------