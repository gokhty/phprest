<?php
require_once("conexion.php");
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['id']))
    {
		$id = $_GET["id"];
      //Mostrar un post
      $sql = $db->prepare("SELECT * FROM productos where id = ?");
      $sql->bindParam(1,$id);
      $sql->execute();
      header("HTTP/1.1 200 OK");
      echo json_encode(  $sql->fetch(PDO::FETCH_ASSOC)  );
      exit();
	  }
    else {
      //Mostrar lista de post
      $sql = $db->prepare("SELECT * FROM productos");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll()  );
      exit();
	}
}

// Crear un nuevo post
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $serie = $_POST["serie"];
	$nombre = $_POST["nombre"];
	$precio = $_POST["precio"];
	$stock = $_POST["stock"];
    $sql = "insert into productos(serie, nombre, precio, stock)values(?,?,?,?)";
    $statement = $db->prepare($sql);
	$statement->bindParam(1,$serie);
    $statement->bindParam(2,$nombre);
	$statement->bindParam(3,$precio);
	$statement->bindParam(4,$stock);
    $statement->execute();
    $postId = $db->lastInsertId();
    if($postId)
    {
      $input['id'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{	
	$id = $_GET["id"];
	$precio = $_GET["precio"];
    $sql = "update productos set precio = ? where id = ?";
    $statement = $db->prepare($sql);
	$statement->bindParam(1,$precio);
    $statement->bindParam(2,$id);
    $statement->execute();
    $postId = $db->lastInsertId();
    if($postId)
    {
      $input['id'] = $postId;
      header("HTTP/1.1 200 OK");
      echo json_encode($input);
      exit();
	 }
}
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{
	$id = $_GET['id'];
  $statement = $db->prepare("DELETE FROM productos where id=?");
  $statement->bindParam(1,$id);
  $statement->execute();
	header("HTTP/1.1 200 OK");
	exit();
}
?>