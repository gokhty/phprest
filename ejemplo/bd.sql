drop database if exists bd;
create database bd;
use bd;
create table juegos(
id int primary key auto_increment,
nombre varchar(45)
);
delimiter |
create procedure sp_insertaJuego(nom varchar(45))
begin
insert into juegos(nombre) values(nom);
end
|
delimiter |
create procedure sp_listaJuego()
begin
select * from juegos;
end
|
call sp_insertaJuego('sc');
select * from juegos;
call sp_listaJuego();