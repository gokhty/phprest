<?php
require_once('../util/conexion.php');
class mysqljuego{
    private $db;
	private $listajuego;
    public function __construct(){
		$bd = new conexion();
		$this->db= $bd->getConexion();
		$this->listajuego=array();
    }
	public function registrar($a){
        $consulta=$this->db->prepare("call sp_insertaJuego(?)");
		$consulta->bindParam(1,$a);
		$consulta->execute();
		$consulta = null;
		$this->db = null; 
    }
	
	public function listar(){
        $consulta=$this->db->prepare("call sp_listaJuego();");
		$consulta->execute();
        while($filas=$consulta->fetch(PDO::FETCH_ASSOC)){
            $this->listajuego[]=$filas;
        }
        return $this->listajuego;
		$consulta = null;
		$this->db = null; 
    }
}
?>