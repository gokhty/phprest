-- drop database h;
create database h;
use h;
create table tb_cliente(
id_cliente int primary key auto_increment,
nombre varchar(200),
apellido varchar(200),
usuario varchar(200),
pass varchar(200),
fecNac date,
genero char(1)
);
create table tb_tarjeta(
id_tarjeta int primary key auto_increment,
n_tarjeta varchar(16),
cvv char(3),
cli int,
foreign key (cli) references tb_cliente(id_cliente)
);
create table producto(
id_producto int primary key auto_increment,
descripcion varchar(45),
precio decimal,
stock int,
imgProducto varchar(45)
);
create table oferta_dia(
id_oferta_dia int primary key auto_increment,
edad int,
genero char(1),
estado char(1)
);
create table tb_perfilCompra(
id_perfilCompra int primary key auto_increment,
id_oferta_dia int,
foreign key (id_oferta_dia) references oferta_dia(id_oferta_dia),
id_producto int,
foreign key (id_producto) references producto(id_producto)
);
create table compra(
idcompra int primary key auto_increment,
idcliente int,
id_perfilcompra int,
fecha date,
foreign key(idcliente)references tb_cliente(id_cliente),
foreign key (id_perfilcompra) references tb_perfilCompra(id_perfilCompra)
);

insert into producto(descripcion, precio, stock, imgProducto)values
('arroz 3k', 9.0, 300, 'img/arroz.jpg');
select * from producto;

insert into oferta_dia(edad, genero,estado) values
(21,'F','1');
select * from oferta_dia;

insert into tb_perfilCompra(id_oferta_dia, id_producto)values
(1,1);
select * from tb_perfilCompra;