-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-04-2019 a las 17:15:48
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `h`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_login`(usu varchar(200), ppass varchar(200))
begin
	if exists(select * from tb_cliente where usuario = usu)then
		select count(*) contar from tb_cliente where usuario = usu and pass = ppass;
	else
		insert into tb_cliente(usuario, pass) values (usu, ppass);
	end if; 
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE IF NOT EXISTS `compra` (
  `idcompra` int(11) NOT NULL,
  `idcliente` int(11) DEFAULT NULL,
  `id_perfilcompra` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta_dia`
--

CREATE TABLE IF NOT EXISTS `oferta_dia` (
  `id_oferta_dia` int(11) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oferta_dia`
--

INSERT INTO `oferta_dia` (`id_oferta_dia`, `id_producto`, `estado`) VALUES
(1, 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `imgProducto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `descripcion`, `precio`, `stock`, `imgProducto`) VALUES
(1, 'galletas a4', '3', 300, 'img/galleta.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_cliente`
--

CREATE TABLE IF NOT EXISTS `tb_cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `usuario` varchar(200) DEFAULT NULL,
  `pass` varchar(200) DEFAULT NULL,
  `fecNac` date DEFAULT NULL,
  `genero` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_cliente`
--

INSERT INTO `tb_cliente` (`id_cliente`, `nombre`, `apellido`, `usuario`, `pass`, `fecNac`, `genero`) VALUES
(1, 'katy', 'hernadez', 'katy', '1234', '1995-01-01', 'F'),
(2, NULL, NULL, 'ana', '1234', NULL, NULL),
(3, 'nina', 'marquez', 'nina', '1234', '1997-01-01', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_perfilcompra`
--

CREATE TABLE IF NOT EXISTS `tb_perfilcompra` (
  `id_perfilCompra` int(11) NOT NULL,
  `id_oferta_dia` int(11) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `genero` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_perfilcompra`
--

INSERT INTO `tb_perfilcompra` (`id_perfilCompra`, `id_oferta_dia`, `edad`, `genero`) VALUES
(1, 1, 21, 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_tarjeta`
--

CREATE TABLE IF NOT EXISTS `tb_tarjeta` (
  `id_tarjeta` int(11) NOT NULL,
  `n_tarjeta` varchar(16) DEFAULT NULL,
  `cvv` char(3) DEFAULT NULL,
  `cli` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_tarjeta`
--

INSERT INTO `tb_tarjeta` (`id_tarjeta`, `n_tarjeta`, `cvv`, `cli`) VALUES
(1, '1234123412341234', '123', 1),
(2, '6523452342345432', '423', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idcompra`),
  ADD KEY `idcliente` (`idcliente`),
  ADD KEY `id_perfilcompra` (`id_perfilcompra`);

--
-- Indices de la tabla `oferta_dia`
--
ALTER TABLE `oferta_dia`
  ADD PRIMARY KEY (`id_oferta_dia`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `tb_cliente`
--
ALTER TABLE `tb_cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `tb_perfilcompra`
--
ALTER TABLE `tb_perfilcompra`
  ADD PRIMARY KEY (`id_perfilCompra`),
  ADD KEY `id_oferta_dia` (`id_oferta_dia`);

--
-- Indices de la tabla `tb_tarjeta`
--
ALTER TABLE `tb_tarjeta`
  ADD PRIMARY KEY (`id_tarjeta`),
  ADD KEY `cli` (`cli`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `oferta_dia`
--
ALTER TABLE `oferta_dia`
  MODIFY `id_oferta_dia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_cliente`
--
ALTER TABLE `tb_cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tb_perfilcompra`
--
ALTER TABLE `tb_perfilcompra`
  MODIFY `id_perfilCompra` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_tarjeta`
--
ALTER TABLE `tb_tarjeta`
  MODIFY `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `tb_cliente` (`id_cliente`),
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`id_perfilcompra`) REFERENCES `tb_perfilcompra` (`id_perfilCompra`);

--
-- Filtros para la tabla `oferta_dia`
--
ALTER TABLE `oferta_dia`
  ADD CONSTRAINT `oferta_dia_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `tb_perfilcompra`
--
ALTER TABLE `tb_perfilcompra`
  ADD CONSTRAINT `tb_perfilcompra_ibfk_1` FOREIGN KEY (`id_oferta_dia`) REFERENCES `oferta_dia` (`id_oferta_dia`);

--
-- Filtros para la tabla `tb_tarjeta`
--
ALTER TABLE `tb_tarjeta`
  ADD CONSTRAINT `tb_tarjeta_ibfk_1` FOREIGN KEY (`cli`) REFERENCES `tb_cliente` (`id_cliente`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
