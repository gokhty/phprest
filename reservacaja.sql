-- drop database caja;
create database caja;
use caja;
create table usuarios(
id_dni char(8) primary key,
nombre varchar(45),
correo varchar(45),
dni char(8),
usu varchar(45),
pass varchar(45)
);
create table cajas(
id_caja int primary key auto_increment
);
create table reservaCajas(
id_dni char(8),
tiempo int, -- tiempo en el que el cliente va ha terminar de comprar
horaAtencion time -- suma de tiempo + hora actual
);
/*
-- obtener una caja según el tiempo que establece el usuario
-- el último atendido - el siguiente después de este: si el resultado es menor a tiempo del siguiente+1 antentido que se antienda antes de este, sino que se antienda despúes
*/
/*
el usuario al escoger prorroga le dan una caja con tiempo mayor a la ultima reservada(ver todas las cajas reservadas)
*/