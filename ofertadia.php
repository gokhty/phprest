<?php
require_once("conexion.php");
$edad = $_GET["edad"];
$genero = $_GET["genero"];
$min = $edad -3;
$max = $edad +3;
$query = $db->prepare('select p.* from tb_perfilCompra pc join oferta_dia od on pc.id_oferta_dia = od.id_oferta_dia join producto p on p.id_producto = pc.id_producto where od.edad between ? and ? and genero = ? and estado = 1');
$query->bindParam(1,$min);
$query->bindParam(2,$max);
$query->bindParam(3,$genero);
$query->execute();
$datos = array();
$cont = 0;
while($res = $query->fetch(PDO::FETCH_ASSOC)){
		
	$datos[$cont]['descripcion'] = utf8_encode($res['descripcion']);
	$datos[$cont]['precio'] = utf8_encode($res['precio']);
	$datos[$cont]['stock'] = utf8_encode($res['stock']);
	$datos[$cont]['imgProducto'] = utf8_encode($res['imgProducto']);
$cont++;
}
$fjson = json_encode($datos);
echo '{"usuarios":'.$fjson."}";
?>